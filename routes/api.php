<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('categories', 'CategoryController@index'); // list categories
Route::post('category', 'CategoryController@store'); // create new category
Route::delete('category/{id}', 'CategoryController@destroy'); // delete todo

Route::get('todos', 'TodoController@index'); // list todos
Route::post('todo', 'TodoController@store'); // create new todo
Route::put('todo', 'TodoController@store'); // Update todo
Route::put('todo/{id}', 'TodoController@change'); // delete todo
Route::delete('todo/{id}', 'TodoController@destroy'); // delete todo
