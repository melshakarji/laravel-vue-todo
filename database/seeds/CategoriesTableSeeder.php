<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $titles = ['shopping','personal','business'];
      foreach ($titles as $title) {
        Category::create([
          'title'            => $title
        ]);
      }
    }
}
