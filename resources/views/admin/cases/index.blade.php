@extends('layout.app')

@section('content')
  <div class="row mt-5">
    <div class="col">
      <h2>Administrate Cases</h2>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <admin-list-cases></admin-list-cases>
    </div>
  </div>
@endsection
